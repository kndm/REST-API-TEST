from django.shortcuts import render
from django.http import HttpResponseBadRequest
from backend.models import Cgroup, Process
from backend.serializers import CgroupSerializer, ProcessSerializer

from rest_framework import views, generics
from rest_framework.response import Response

import subprocess
import os


class CgroupList(generics.ListCreateAPIView):
    queryset = Cgroup.objects.all()
    serializer_class = CgroupSerializer

    def post(self, request, *args, **kwargs):
        group_name = request.data.get('group_name', None)
        group_memory_limit = request.data.get('memory_limit', None)

        if group_name and group_memory_limit:
            cmd = 'mkdir /sys/fs/cgroup/memory/' + group_name + '/'
            memory_cmd = 'echo ' + \
                str(group_memory_limit) + ' > /sys/fs/cgroup/memory/' + \
                group_name + '/memory.limit_in_bytes'
            subprocess.call(cmd, shell=True)

            # Check if the folder was created succesfully, if not return False
            if os.path.exists('/sys/fs/cgroup/memory/' + group_name + '/'):
                subprocess.call(memory_cmd, shell=True)
                cgroup = Cgroup.objects.create(
                    group_name=group_name, memory_limit=group_memory_limit)

                return Response({"success": True})
            else:
                return Response({"success": False})

        else:
            return HttpResponseBadRequest()


class CgroupDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Cgroup.objects.all()
    serializer_class = CgroupSerializer


class ProcessList(generics.ListCreateAPIView):
    queryset = Process.objects.all()
    serializer_class = ProcessSerializer

    def post(self, request, *args, **kwargs):
        cgroup = request.data.get('cgroup')
        process_id = request.data.get('process_id', None)
        cgroup_fetch = Cgroup.objects.get(pk=cgroup)

        if cgroup_fetch and int(process_id) > 0:
            cmd = 'echo ' + str(process_id) + ' > /sys/fs/cgroup/memory/' + \
                cgroup_fetch.group_name + '/cgroup.procs'
            subprocess.call(cmd, shell=True)

            # Check if the file exists, if not return False
            if os.path.exists('/sys/fs/cgroup/memory/' + cgroup_fetch.group_name + '/cgroup.procs'):

                new_process = Process.objects.create(
                    cgroup=cgroup_fetch, process_id=process_id)

                return Response({"success": True})
            else:
                return Response({"success": False})

        else:
            return HttpResponseBadRequest()


class ProcessDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Process.objects.all()
    serializer_class = ProcessSerializer
