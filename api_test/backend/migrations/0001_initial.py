# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2018-01-19 08:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cgroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('group_name', models.CharField(max_length=50)),
                ('memory_limit', models.IntegerField()),
                ('date_created', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['group_name'],
            },
        ),
        migrations.CreateModel(
            name='Process',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('process_id', models.IntegerField()),
                ('date_created', models.DateTimeField(auto_now=True)),
                ('cgroup', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='backend.Cgroup')),
            ],
        ),
    ]
