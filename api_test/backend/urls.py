from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from backend import views

urlpatterns = [

	url(r'^api/v1/cgroup/$', views.CgroupList.as_view(), name='get_post_cgroup'),
	url(r'^api/v1/cgroup/(?P<pk>[0-9]+)/$', views.CgroupDetail.as_view(), name='get_delete_update_cgroup'),
	url(r'^api/v1/process/$', views.ProcessList.as_view(), name='get_post_process'),
	url(r'^api/v1/process/(?P<pk>[0-9]+)/$', views.ProcessDetail.as_view(), name='get_delete_update_process'),


]

urlpatterns = format_suffix_patterns(urlpatterns)