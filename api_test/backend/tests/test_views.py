import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from backend.models import Cgroup, Process
from backend.serializers import CgroupSerializer, ProcessSerializer

import subprocess
import os

client = Client()


class GetAllCgroupsTest(TestCase):
    """ Test module for GET cgroups API call """

    def setUp(self):
        Cgroup.objects.create(group_name="TestGroup1", memory_limit=1000)
        Cgroup.objects.create(group_name="TestGroup2", memory_limit=1000)
        Cgroup.objects.create(group_name="TestGroup3", memory_limit=1000)

    def test_get_all_cgroups(self):
        # get API response
        response = client.get(reverse('get_post_cgroup'))
        # fetch data from db
        cgroups = Cgroup.objects.all()
        serializer = CgroupSerializer(cgroups, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleCgroupTest(TestCase):
    """ Test module for GET single cgroup API call """

    def setUp(self):
        self.test_group = Cgroup.objects.create(
            group_name="TestGroup1", memory_limit=1000)

    def test_get_valid_cgroup(self):
        response = client.get(
            reverse('get_delete_update_cgroup', kwargs={'pk': self.test_group.pk}))
        cgroup = Cgroup.objects.get(pk=self.test_group.pk)
        serializer = CgroupSerializer(cgroup)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_cgroup(self):
        response = client.get(
            reverse('get_delete_update_cgroup', kwargs={'pk': 20}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateFileCgroupsTest(TestCase):
    """ Test for Cgroup creation via mkdir sys/fs """

    def test_create_cgroups(self):
        group_name = "TestGroup"
        cmd = 'mkdir /sys/fs/cgroup/memory/' + group_name + '/'
        subprocess.call(cmd, shell=True)
        self.assertTrue(os.path.exists(
            '/sys/fs/cgroup/memory/' + group_name + '/'))


class PostCgroupsTest(TestCase):
    """ Test for POST Cgroup """

    def setUp(self):
        self.valid_payload = {
            'group_name': 'TestGroup1',
            'memory_limit': 100,
        }
        self.invalid_payload = {
            'group_name': '',
            'memory_limit': 1000,
        }

    def test_create_valid_cgroup(self):
        response = client.post(
            reverse('get_post_cgroup'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_invalid_cgroup(self):
        response = client.post(
            reverse('get_post_cgroup'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class GetAllProcessTest(TestCase):
    """ Test module for GET process API call """

    def setUp(self):
        Cgroup.objects.create(group_name='TestGroup1', memory_limit=1000)
        cgroup = Cgroup.objects.get(group_name='TestGroup1')
        Process.objects.create(cgroup=cgroup, process_id=123)
        Process.objects.create(cgroup=cgroup, process_id=456)

    def test_get_all_cgroups(self):
        # get API response
        response = client.get(reverse('get_post_process'))
        # fetch data from db
        process = Process.objects.all()
        serializer = ProcessSerializer(process, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleProcessTest(TestCase):
    """ Test module for GET single cgroup API call """

    def setUp(self):
        self.test_group = Cgroup.objects.create(
            group_name="TestGroup1", memory_limit=1000)
        self.test_group = Cgroup.objects.get(group_name="TestGroup1")
        self.test_process = Process.objects.create(
            cgroup=self.test_group, process_id=1234)

    def test_get_valid_process(self):
        response = client.get(
            reverse('get_delete_update_process', kwargs={'pk': self.test_process.pk}))
        process = Process.objects.get(pk=self.test_process.pk)
        serializer = ProcessSerializer(process)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_process(self):
        response = client.get(
            reverse('get_delete_update_process', kwargs={'pk': 20}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateFileProcessTest(TestCase):
    """ Test for Process creation via echo sys/fs """

    def setUp(self):
        Cgroup.objects.create(group_name='TestGroup1', memory_limit=1000)
        cgroup = Cgroup.objects.get(group_name='TestGroup1')
        self.group_name = cgroup.group_name
        file_creation = 'mkdir /sys/fs/cgroup/memory/' + self.group_name + '/'
        subprocess.call(file_creation, shell=True)

    def test_create_process(self):
        process_id = 123
        cmd = 'echo ' + str(process_id) + ' > /sys/fs/cgroup/memory/' + \
            self.group_name + '/cgroup.procs'
        subprocess.call(cmd, shell=True)
        self.assertTrue(os.path.exists(
            '/sys/fs/cgroup/memory/' + self.group_name + '/cgroup.procs'))


class PostProcessTest(TestCase):
    """ Test for POST Process """

    def setUp(self):
        Cgroup.objects.create(group_name='TestGroup1', memory_limit=1000)

        self.valid_payload = {
            'cgroup': 1,
            'process_id': 123,
        }
        self.invalid_payload = {
            'cgroup': 1,
            'process_id': -23,
        }

    def test_create_valid_cgroup(self):
        response = client.post(
            reverse('get_post_process'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_invalid_cgroup(self):
        response = client.post(
            reverse('get_post_process'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
