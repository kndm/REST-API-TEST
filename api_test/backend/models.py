import uuid
from django.db import models

class Cgroup(models.Model):
	group_name = models.CharField(max_length=50, blank=False, null=False)
	memory_limit = models.IntegerField(blank=False, null=False)
	date_created = models.DateTimeField(auto_now=True)

	class Meta:
		ordering = ['group_name']

	def __str__(self):
		return self.group_name

class Process(models.Model):
	cgroup = models.ForeignKey('Cgroup', on_delete=models.CASCADE, blank=False)
	process_id = models.IntegerField(blank=False, null=False)
	date_created = models.DateTimeField(auto_now=True)

	def __str__(self):
		return str(self.process_id)

	@property
	def process_set(self):
		return str(self.process_id)



