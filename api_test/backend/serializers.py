from rest_framework import serializers
from backend.models import Cgroup, Process

class ProcessSerializer(serializers.ModelSerializer):
	class Meta:
		model = Process
		fields = '__all__'

class CgroupSerializer(serializers.ModelSerializer):
	process_set = serializers.StringRelatedField(many=True, read_only=True)
	class Meta:
		model = Cgroup
		fields = '__all__'