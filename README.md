# Cgroup Manager REST API

The following API was created to satisfy the following requirements:

RESTful API using Django Rest Framework to manage cgroups of a server. 

**The API should be able to**:
- Create a new cgroup
- Place a process (PID) into a cgroup
- List the tasks (PIDs) for a given cgroup
 - The code will run on CentOS 7.4 with a default setup and 3.10 kernel.
- Cgroups are mounted via sysfs.
- No authentication is required.

# Requirements
- Python3
- Django REST Framework
- Django 1.10

# Installation

1. Clone this repository
2. Install requirements via pip using the following command on a terminal:
	> pip install -r requirements.txt
3. Make migrations via terminal with the following commands:
	> python manage.py makemigrations
	> python manage.py migrate
4. Start the server via terminal with the following command:
	> python manage.py runserver


# Usage
Currently the API does not have an API root, however all of the endpoints are accessible.

>/api/v1/cgroup/
>
>/api/v1/process/

Once the server is running these can be accessed through the following URL:

http://127.0.0.1:8000/api/v1/cgroup/
http://127.0.0.1:8000/api/v1/process/

POST requests can be executed via the API's interface (located at the bottom of the page)

	
# Running Tests
All tests can be run via terminal using the following command:
> python manage.py test backend.tests

